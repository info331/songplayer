import java.util.*;

public class SongCatalog {
    private List<String> songs = Arrays.asList("yesterday", "let it be", "i feel fine");

    public String search(String name) {
        for (String song : songs)
            if (song.startsWith(name))
                return song;

        return null;
    }
}
