import java.util.ArrayList;

public class SongController {

    public ArrayList<String> recentSongs = new ArrayList<>();
    private SongCatalog catalog;
    private SongPlayer player;

    public SongController(SongCatalog catalog, SongPlayer player) {
        this.catalog = catalog;
        this.player = player;
    }

    public void play (String name) {
        String fullName = catalog.search(name);
        if (fullName != null) {
            recentSongs.add(0, fullName);
            player.play(fullName);
        }
    }

    public void playLast () {
        if (!recentSongs.isEmpty())
            player.play(recentSongs.get(0));
   }

    public void printRecentSongs() {
        for (String song : recentSongs)
            System.out.println(song);
    }
}
