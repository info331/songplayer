import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {

    private SongController controller;
    private SongCatalog catalog;
    private SongPlayer player;

    public static void main (String[] args) {
        Application app = new Application();
        app.run();
    }

    private void run () {
        try {
            catalog = new SongCatalog();
            player = new SongPlayer();
            controller = new SongController(catalog, player);

            System.out.println("Hello!");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                String command = bufferedReader.readLine();
                processCommand (command);
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void processCommand (String command) {
        if (command.startsWith("play")) {
            String songName = command.split(" ")[1];
            controller.play(songName);
        }
        else if (command.equals("last")) {
            controller.playLast();
        }
        else if (command.equals("recent"))
            controller.printRecentSongs();
        else {
            System.out.println("Unknown command");
        }
    }
}
